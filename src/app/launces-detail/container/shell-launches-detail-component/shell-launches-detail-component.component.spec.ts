import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EffectsModule } from '@ngrx/effects';
import { Store, StoreModule } from '@ngrx/store';
import { MockStore, provideMockStore, } from '@ngrx/store/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { AppEffects } from '../../../state/effects';
import { ShellLaunchesDetailComponentComponent } from './shell-launches-detail-component.component';
import * as actions from '../../../state/actions';
import { reducer } from '../../../state/reducers';
import * as storeSubcribe from '../../../state/selectors';

describe('ShellLaunchesDetailComponentComponent', () => {
  let component: ShellLaunchesDetailComponentComponent;
  let fixture: ComponentFixture<ShellLaunchesDetailComponentComponent>;
  let store: MockStore<any>;
  const initialState = {
    launchesList: [],
    errorResponse: null
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShellLaunchesDetailComponentComponent],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        StoreModule.forRoot(
          { spacex: reducer },
          {
            runtimeChecks: {
              strictStateImmutability: true,
              strictActionImmutability: true
            }
          }),
        EffectsModule.forRoot([]),
        EffectsModule.forFeature([AppEffects]),
      ],
      providers: [
        provideMockStore({
          initialState,
          selectors: [
            { selector: storeSubcribe.getLaunches, value: [] }
          ]
        })
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();

    store = TestBed.inject(MockStore);
    spyOn(store, 'dispatch').and.callThrough();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShellLaunchesDetailComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch getLaunches action', () => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const action = actions.getLaunches({ filter: {} });
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });
  });


});
