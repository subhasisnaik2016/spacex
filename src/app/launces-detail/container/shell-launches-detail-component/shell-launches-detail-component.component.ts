/**
 * Page Summary : 
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { select, Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import * as actions from '../../../state/actions';
import * as storeSubcribe from '../../../state/selectors';

@Component({
  selector: 'spaceX-shell-launches-detail-component',
  templateUrl: './shell-launches-detail-component.component.html'
})
export class ShellLaunchesDetailComponentComponent implements OnInit, OnDestroy {

  /** Observable which is connected with store for search results */
  launchesList$: Observable<any>;

  /** For unsubscribing Observables */
  ngUnsubscribe = new Subject<void>();

  /** Initial filter values */
  filterState = {
    launch_success: null,
    land_success: null,
    launch_year: null,
    limit: 100
  };

  /**
   *
   * @param store client side data store
   * @param activatedRoute contains various information about current route
   * @param router A service that provides navigation among views and URL manipulation capabilities
   */
  constructor(private store: Store<any>, private activatedRoute: ActivatedRoute, private router: Router) { }

  /**
   * Component life cycle hook
   */
  ngOnInit(): void {
    this.launchesList$ = this.store.pipe(select(storeSubcribe.getLaunches));
    this.activatedRoute.queryParams.pipe(takeUntil(this.ngUnsubscribe)).subscribe(params => {
      this.filterState = {
        launch_success: null,
        land_success: null,
        launch_year: null,
        limit: 100
      };
      this.searchWithQryPrm(params);
    });
  }

  /**
   * This function checks current route query params &
   * trigger search based on those param filter on route change
   * @param params Contains route params
   */
  private searchWithQryPrm(params: any) {
    if (params && params.launch_success !== undefined) {
      this.filterState.launch_success = params.launch_success === 'true';
    }
    if (params && params.land_success !== undefined) {
      this.filterState.land_success = params.land_success === 'true';
    }
    if (params && params.launch_year) {
      this.filterState.launch_year = params.launch_year;
    }
    const fltObj = this.getAppliedFilterObj(this.filterState);
    this.searchLaunches(fltObj);
  }

  /**
   * Dispatch action with params for search
   * @param filter search params
   */
  private searchLaunches(filter) {
    this.store.dispatch(actions.getLaunches({ filter }));
  }

  /**
   * This function triggers search for every filter change
   * by navigating to route with params
   * @param filter search params
   */
  changeFilter(filter) {
    if (filter) {
      const fltObj = this.getAppliedFilterObj(filter);
      this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: fltObj });
    }
  }

  /**
   * Returns a object with keys which has value
   * @param filter search params
   */
  private getAppliedFilterObj(filter) {
    const filteredData = {};
    for (const key in filter) {
      if (Object.prototype.hasOwnProperty.call(filter, key)) {
        const element = filter[key];
        if (element !== null) {
          filteredData[key] = element;
        }
      }
    }
    return filteredData;
  }

  /**
   * Unsubscribe active subscription
   */
  ngOnDestroy() {
    if (this.ngUnsubscribe) {
      this.ngUnsubscribe.next();
      this.ngUnsubscribe.complete();
    }
  }
}
