import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectsModule } from '@ngrx/effects';
import { Store, StoreModule } from '@ngrx/store';
import { MockStore, provideMockStore, } from '@ngrx/store/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { AppEffects } from '../../../state/effects';
import * as actions from '../../../state/actions';
import { reducer } from '../../../state/reducers';
import * as storeSubcribe from '../../../state/selectors';
import { FilterComponentComponent } from './filter-component.component';

describe('FilterComponentComponent', () => {
  let component: FilterComponentComponent;
  let fixture: ComponentFixture<FilterComponentComponent>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FilterComponentComponent],
      imports: [],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should generate years', () => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      component.generateYear();
      expect(component.years.length).toBeGreaterThanOrEqual(1);
    });
  });

  it('should change filter', () => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      component.filterState = {
        launch_success: true,
        land_success: null,
        launch_year: null,
        limit: 100
      };
      component.filterChange('launch_success', false);
      expect(component.filterState.launch_success).toEqual(false);
      component.filterChange('launch_success', false);
      expect(component.filterState.launch_success).toEqual(null);
    });
  });
});
