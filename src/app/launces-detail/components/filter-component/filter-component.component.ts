/**
 * Page summery : This component contains all the filters & notify parent component for every filter change
 */

import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'spaceX-filter-component',
  templateUrl: './filter-component.component.html',
  styleUrls: ['./filter-component.component.scss']
})
export class FilterComponentComponent implements OnInit, OnChanges {

  /** Emit event on every filter value change */
  @Output() emitFilterChange: EventEmitter<any> = new EventEmitter();

  /** Initial values for filter based on page load */
  @Input() initialState: any;

  /** Initial value */
  filterState = {
    launch_success: null,
    land_success: null,
    launch_year: null,
    limit: 100
  };

  /** Years array to show in filter */
  years = [];

  constructor() { }

  /**
   * Component life cycle hook
   */
  ngOnInit(): void {
    this.generateYear();
  }

  /**
   * Component life cycle hook
   * Triggered when ever value changes
   */
  ngOnChanges(simpleChange: SimpleChanges) {
    if (this.initialState && simpleChange.initialState) {
      this.filterState = { ...this.initialState };
    }
  }

  /**
   * Generate years for filter
   */
  generateYear() {
    for (let index = 2006; index < 2021; index++) {
      this.years.push(`${index}`);
    }
  }

  /**
   * Handles every filter change & assign value based
   * on selection to its respective key
   * @param key filter key
   * @param value filter value
   */
  filterChange(key: string, value: any) {
    if (this.filterState[key] === value) {
      this.filterState[key] = null;
    } else {
      this.filterState[key] = value;
    }
    this.emitFilterChange.emit({ ...this.filterState });
  }
}
