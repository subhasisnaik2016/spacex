/**
 * Page summery : This component lists all the launches coming from Input
 */

import { Component, Input } from '@angular/core';

@Component({
  selector: 'spaceX-launches-detail-component',
  templateUrl: './launches-detail-component.component.html',
  styleUrls: ['./launches-detail-component.component.scss']
})
export class LaunchesDetailComponentComponent {

  /** Launches List */
  @Input() launchesList: any;

  constructor() { }

}
