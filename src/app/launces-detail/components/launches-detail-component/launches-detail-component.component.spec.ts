import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LaunchesDetailComponentComponent } from './launches-detail-component.component';

describe('LaunchesDetailComponentComponent', () => {
  let component: LaunchesDetailComponentComponent;
  let fixture: ComponentFixture<LaunchesDetailComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LaunchesDetailComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchesDetailComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
