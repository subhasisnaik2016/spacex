import { TestBed } from '@angular/core/testing';

import { LauncesDetailService } from './launces-detail.service';

describe('LauncesDetailService', () => {
  let service: LauncesDetailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LauncesDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
