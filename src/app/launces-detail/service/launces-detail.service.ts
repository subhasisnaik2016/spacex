import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LauncesDetailService {

  constructor(private http: HttpClient) { }

  getLaunches(params: any) {
    return this.http.get(`${environment.baseUrl}/launches`, { params }).pipe(
      map(events => {
        return events;
      }),
      catchError(this.handleError)
    );

  }

  handleError(error: Response) {
    return throwError(error);
  }
}
