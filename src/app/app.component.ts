/**
 * Page Summary : This is the parent component which contains Headers, Footers & Main section
 */

import { Component } from '@angular/core';

@Component({
  selector: 'spaceX-root',
  templateUrl: './app.component.html'
})
export class AppComponent { }
