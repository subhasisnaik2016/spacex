import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShellLaunchesDetailComponentComponent } from './launces-detail/container/shell-launches-detail-component/shell-launches-detail-component.component';
import {
  LaunchesDetailComponentComponent
} from './launces-detail/components/launches-detail-component/launches-detail-component.component';
import { FilterComponentComponent } from './launces-detail/components/filter-component/filter-component.component';
import { StoreModule } from '@ngrx/store';
import { reducer } from './state/reducers';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppEffects } from './state/effects';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ShellLaunchesDetailComponentComponent,
    LaunchesDetailComponentComponent,
    FilterComponentComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    StoreModule.forRoot(
      { spacex: reducer },
      {
        runtimeChecks: {
          strictStateImmutability: true,
          strictActionImmutability: true
        }
      }),
    EffectsModule.forRoot([
      AppEffects
    ]),
    StoreDevtoolsModule.instrument({
      name: 'Spacex App',
      maxAge: 25
    }),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
