import { createFeatureSelector, createSelector } from '@ngrx/store';

const reducerFeatureState = createFeatureSelector<any>('spacex');

export const getLaunches = createSelector(reducerFeatureState, state => {
    return state.spacexReducer.launchesList;
});

export const getErrorRes = createSelector(reducerFeatureState, state => {
    return state.spacexReducer.errorResponse;
});
