import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { LauncesDetailService } from '../../launces-detail/service/launces-detail.service';
import * as action from '../actions';


@Injectable()
export class AppEffects {
    constructor(private action$: Actions, private service: LauncesDetailService) { }

    getLaunches$: Observable<Action> =
        createEffect(() =>
            this.action$.pipe(
                ofType(action.getLaunches),
                switchMap((parm) => this.service.getLaunches(parm.filter).pipe(
                    map((response: any) => {
                        return action.getLaunchesSuccess({ response });
                    }),
                    catchError(error => of(action.failure(error)))
                )
                )
            )
        );
}
