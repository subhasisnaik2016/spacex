import { ActionReducer, combineReducers, compose } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';

import { LaunchesReducer } from './launches.reducer';

const reducers = {
    spacexReducer: LaunchesReducer
};

const developmentReducer: ActionReducer<any> = compose(storeFreeze, combineReducers)(reducers);

export function reducer(state: any, action: any) {
    return developmentReducer(state, action);
}
