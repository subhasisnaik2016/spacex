import { createReducer, on } from '@ngrx/store';
import * as appAction from '../actions';

export const initialState: any = {
    launchesList: [],
    errorResponse: null
};

export const LaunchesReducer = createReducer(initialState,

    on(appAction.getLaunchesSuccess, (state, action) => ({
        ...state,
        launchesList: action.response
    })),
    on(appAction.failure, (state, action) => ({
        ...state,
        launchesList: [],
        errorResponse: action.error
    })),
);
