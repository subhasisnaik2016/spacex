import { createAction, props } from '@ngrx/store';


export const getLaunches = createAction(
    '[spacex-action] getLaunches',
    props<{ filter: any }>()
);

export const getLaunchesSuccess = createAction(
    '[spacex-action] getLaunchesSuccess',
    props<{ response: any }>()
);

export const failure = createAction(
    '[spacex-action] failure',
    props<{ error: any }>()
);