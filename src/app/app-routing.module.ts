import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  ShellLaunchesDetailComponentComponent
} from './launces-detail/container/shell-launches-detail-component/shell-launches-detail-component.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'launches',
    pathMatch: 'full',
  },
  {
    path: 'launches',
    component: ShellLaunchesDetailComponentComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
